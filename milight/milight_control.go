package milight

const (
	FIRST_BYTE_OF_REMOTE = "3D"
	SECOND_BYTE_OF_REMOTE = "9B"
)

func SwitchBulbOn(id int) {
	keys := map[int]string{
		1: "3",
		2: "5",
		3: "7",
		4: "9",
	}
	key := keys[id]
	command := BuildNewMilightCommand(FIRST_BYTE_OF_REMOTE, SECOND_BYTE_OF_REMOTE, key)
	logger.Debug("Switching Bulb %d on (%+v)...", id, command)
	sendMilightCommand(command)
}

func SwitchBulbOff(id int) {
	keys := map[int]string{
		1: "4",
		2: "6",
		3: "8",
		4: "A",
	}
	key := keys[id]
	command := BuildNewMilightCommand(FIRST_BYTE_OF_REMOTE, SECOND_BYTE_OF_REMOTE, key)
	logger.Debug("Switching Bulb %d off (%+v)...", id, command)
	sendMilightCommand(command)
}
