package milight

import (
	"os/exec"
	"fmt"
)

type milightCommand struct {
	FadeMode           bool
	StrobeMode         bool
	Resends            int
	PrefixValue        string
	FirstByteOfRemote  string
	SecondByteOfRemote string
	ColorByte          string
	BrightnessByte     string
	KeyByte            string
	SequenceByte       string
}

func BuildNewMilightCommand(firstByteOfRemote string,
		secondByteOfRemote string, keyByte string) milightCommand {
	return milightCommand{
		FadeMode:           false,
		StrobeMode:         false,
		Resends:            10,
		PrefixValue:        "B0",
		FirstByteOfRemote:  firstByteOfRemote,
		SecondByteOfRemote: secondByteOfRemote,
		ColorByte:          "00",
		BrightnessByte:     "00",
		KeyByte:            keyByte,
		SequenceByte:       "00",
	}
}

func sendMilightCommand(command milightCommand) {
	cmd := "openmilight"
	args := buildOpenmilightArgs(command)
	if err := exec.Command(cmd, args...).Run(); err != nil {
		logger.Error("Cannot send milight command (%v): %v", command, err)
	}
}
func buildOpenmilightArgs(command milightCommand) []string {
	args := []string{
		"-n", fmt.Sprintf("%d", command.Resends),
		"-p", command.PrefixValue,
		"-q", command.FirstByteOfRemote,
		"-r", command.SecondByteOfRemote,
		"-c", command.ColorByte,
		"-b", command.BrightnessByte,
		"-k", command.KeyByte,
		"-v", command.SequenceByte,
	}
	if command.FadeMode {
		args = append(args, "-f")
	}
	if command.StrobeMode {
		args = append(args, "-s")
	}
	return args
}
