package milight

import (
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/sharedapi/context"
)

var (
	ctx        *context.Context
	logger     logging.Logger
)

func Initialize(c *context.Context) {
	ctx = c
	logger = (*ctx.GetService("Logger")).(logging.Logger)
}
