package actuators

import (
	"reflect"
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators/states"
	"bitbucket.org/zaphome/sharedapi/messaging/communication"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators"
)

var (
	ctx        *context.Context
	adapter    *adapters.Adapter
	messageBus *communication.MessageBus
	logger     logging.Logger
)

func Initialize(c *context.Context) {
	ctx = c
	logger = (*ctx.GetService("Logger")).(logging.Logger)
	if a, ok := (*ctx.GetService("Adapter")).(adapters.Adapter); ok {
		adapter = &a
		messageBus = a.MessageBus
		messageBus.DestinationChannel.AddObserverFunction(
			reflect.TypeOf(&states.ActuatorStateChangeInstruction{}),
			processActuatorChangeMessage,
		)
	} else {
		logger.Error("Failed to initialize development actuators.")
	}
}

func AddMilightActuator(name string, state states.State) {
	actuator := actuators.Actuator{
		Id:        0,
		AdapterId: adapter.Id,
		Name:      name,
		State:     state,
	}
	adapter.MessageBus.SourceChannel.SendMessage(
		actuators.BuildActuatorConnectedEvent(adapter.Id, actuator),
	)
}
