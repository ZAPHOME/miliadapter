package actuators

import (
	"bitbucket.org/zaphome/sharedapi/messaging"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators/states"
	"bitbucket.org/zaphome/miliadapter/milight"
)

func processActuatorChangeMessage(message messaging.Message) {
	instruction := message.(*states.ActuatorStateChangeInstruction)
	targetStateAttributes := instruction.State.(map[string]interface{})
	targetSwitchedOn := targetStateAttributes["SwitchedOn"]
	if on := targetSwitchedOn.(bool); on {
		milight.SwitchBulbOn(instruction.ActuatorId)
	} else {
		milight.SwitchBulbOff(instruction.ActuatorId)
	}
	changedEvent := states.BuildActuatorStateChangedEvent(
		adapter.Id, instruction.ActuatorId, instruction.State)
	adapter.MessageBus.SourceChannel.SendMessage(changedEvent)
}
