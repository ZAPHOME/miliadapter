package main

import (
	"bitbucket.org/zaphome/adapterbase/configuration"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators/states"
	"bitbucket.org/zaphome/miliadapter/actuators"
	"bitbucket.org/zaphome/miliadapter/milight"
)

const AdapterName = "MiLight"

var ctx *context.Context

func Initialize(adapt adapters.Adapter) adapters.Adapter {
	adapt.Name = AdapterName

	ctx = configuration.Init(&adapt)
	actuators.Initialize(ctx)
	milight.Initialize(ctx)

	createTestsActuators()
	return adapt
}

func createTestsActuators() {
	actuators.AddMilightActuator("RGBW1", states.OnOffState{ SwitchedOn: false })
	actuators.AddMilightActuator("RGBW2", states.OnOffState{ SwitchedOn: false })
	actuators.AddMilightActuator("RGBW3", states.OnOffState{ SwitchedOn: false })
	actuators.AddMilightActuator("RGBW4", states.OnOffState{ SwitchedOn: false })
}
